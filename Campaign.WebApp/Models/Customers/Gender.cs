﻿namespace Campaign.WebApp.Models.Customers
{
    public enum Gender
    {
        Unknown = 0,
        Male = 1,
        Female = 2,
        NotApplicable = 9
    }
}