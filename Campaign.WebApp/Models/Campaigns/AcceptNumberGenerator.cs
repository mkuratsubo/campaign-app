﻿using System.Transactions;

namespace Campaign.WebApp.Models.Campaigns
{
    internal sealed class AcceptNumberGenerator
    {
        private readonly IAcceptNumberRepository _acceptNumberRepository;

        public AcceptNumberGenerator(IAcceptNumberRepository acceptNumberRepository)
        {
            _acceptNumberRepository = acceptNumberRepository;
        }

        public int NewNumber()
        {
            using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                var previous = _acceptNumberRepository.FindPrevious();
                var @new = previous + 1;
                _acceptNumberRepository.SavePrevious(@new);

                transaction.Complete();

                return @new;
            }
        }
    }
}