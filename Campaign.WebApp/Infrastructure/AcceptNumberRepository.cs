﻿using System.Linq;
using Campaign.WebApp.Models.Campaigns;

namespace Campaign.WebApp.Infrastructure
{
    internal sealed class AcceptNumberRepository : IAcceptNumberRepository
    {
        private readonly ApplicationDbContext _db;

        public AcceptNumberRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public int FindPrevious()
        {
            return _db.AcceptNumber.FirstOrDefault()?.Value ?? 0;
        }

        public void SavePrevious(int value)
        {
            var numbers = _db.AcceptNumber.ToArray();
            _db.AcceptNumber.RemoveRange(numbers);
            _db.AcceptNumber.Add(new AcceptNumberRow {Value = value});
            _db.SaveChanges();
        }
    }
}