﻿namespace Campaign.WebApp.Infrastructure
{
    public class AcceptNumberRow
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
}