﻿using System;
using Campaign.WebApp.Models.Campaigns;
using Campaign.WebApp.Models.Customers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Campaign.WebApp.Infrastructure
{
    public sealed class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public DbSet<Customer> Customers { get; private set; }
        public DbSet<AcceptNumberRow> AcceptNumber { get; private set; }
        public DbSet<CampaignApplication> CampaignApplications { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var birthDateConverter =
                new ValueConverter<BirthDate, DateTime>(v => v.ToDateTime(), v => new BirthDate(v));

            modelBuilder.Entity<Customer>()
                .Property(entity => entity.BirthDate).HasConversion(birthDateConverter);

            modelBuilder.Entity<CampaignApplication>()
                .HasKey(entity => entity.AcceptNumber);
        }
    }
}